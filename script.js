let products = [
    {href: 'product1.html', text: 'Maison Margiela Replica Sneakers', images: ['replica1.jpg','replica4.jpg'], price: '49500 &#8381', info: 'Лаконичные кеды Replica'},
    {href: 'product2.html', text: 'Maison Margiela Tabi Slippers', images: ['tabi2.jpg', "tabi1.jpg"], price:'69500 &#8381', info: 'Вдохновлены традиционными японскими носками'},
    {href: 'product3.html', text: 'OFF-WHITE Out Of Office',images: ['office11.jpg',"office2.jpg"], price: '47850 &#8381', info: 'Эти кеды выглядят так, словно на них в готовом виде расписались маркером' },
    {href: 'product4.html', text: 'New Balance 990',images: ['nb990.jpg', "nb990_2.jpg"], price: '19500 &#8381', info: 'Культовый силуэт вне времени от американского бренда' },
    {href: 'product5.html', text: 'Maison Margiela Runner Evolution',images:['evolution1.jpg','evolution2.jpg'], price: '69950 &#8381', info: 'Многослойный, будто собранный из двух пар обуви верх <br> изготовили из легкого матового полиамида<br> и особенно прочной воловьей кожи' },
    {href: 'product6.html', text: 'Loro Piana Summer Walk', images:['loro1.jpg','loro2.jpg'] , price: '87 450 &#8381', info: ' знаковые лоферы Summer Walk, стилизованные под обувь яхтсменов.'},
    {href: 'product7.html', text: 'Balenciaga Track2', images: ['track1.jpg','track2.jpg'], price: '103000 &#8381', info: 'Для их создания потребовалось 96 элементов:<br> например, массивная каучуковая подошва состоит из девяти частей.<br> Дизайнер привил паре гены беговой обуви и хайкеров:'},
    {href: 'product8.html', text: 'AMIRI Stadium Low', images: ['amiri1.jpg','amiri2.jpg'], price: '77900 &#8381', info: 'Чистый белый цвет и черный фигурный задник,<br> помогут кедам Stadium low поладить с любой одеждой и при этом не раствориться в образе.'},
    {href: 'product9.html', text: 'GOLDEN GOOSE SUPERSTAR', images: ['goose1.jpg','goose2.jpg'], price: '49700 &#8381', info: 'Кеды Superstar выполнили в характерной для бренда эстетике, используя сочетание разных по фактуре материалов.'},
    {href: 'product10.html', text: 'Prada District perforated leather sneakers', images: ['prada1.jpg','prada2.jpg'], price: '63000 &#8381', info: 'Cпортивные кроссовки из перфорированной кожи<br>украшены различными интерпретациями логотипа Prada, который появляется на язычке'},
    {href: 'product11.html', text: 'Dior B30', images: ['b30_1.jpg','b30_2.jpg'], price: '105000 &#8381', info: 'Сникеры B30 в современном спортивном стиле стали новой ключевой моделью модного дома.'},
    {href: 'product12.html', text: 'Dior B23', images: ['b23_1.jpg','b23_2.jpg'], price: '110000 &#8381', info: 'Высокие сникеры B23 представлены в новой интерпретации<br> из прозрачной парусины с мотивом рельефным Dior Oblique,<br> выполненным из белой резины методом инжектирования. '},
    {href: 'product13.html', text: 'Dior B23 Low', images: ['b23_3.jpg','b23_4.jpg'], price: '97000 &#8381', info: 'Низкие сникеры B23 отличаются мотивом Dior Oblique<br> белого и черного цветов и прозрачными вставками.'},
    {href: 'product14.html', text: 'Dior B29', images: ['b29_1.jpg','b29_2.jpg'], price: '99000 &#8381', info: 'Новые сникеры B29 этого сезона отличаются ретро-дизайном в спортивном стиле.<br> Модель сочетает в себе элементы из гладкой телячьей кожи и замши,<br> а также техническую сетку серого цвета.'},
    {href: 'product15.html', text: 'Dior B27 High', images: ['b27_1.jpg','b27_2.jpg'], price: '99000 &#8381', info: 'Высокие сникеры B27 стали неотъемлемой моделью модного дома.<br> Они выполнены из черной гладкой телячьей кожи<br> и украшены вставками из замши и кожи Dior Oblique Galaxy в тон.'},
    {href: 'product16.html', text: 'Dior B27 Low', images: ['b27_3.jpg','b27_4.jpg'], price: '105000 &#8381', info: 'Сникеры дополнены символом "CD Icon" на люверсах,<br> а также несколькими фирменными деталями на двухцветной резиновой подошве,язычке и заднике.<br> Низкие сникеры станут элегантным дополнением вашего любимого образа.'},
    {href: 'product17.html', text: 'Dior B01', images: ['b01_1.jpg','b01_2.jpg'], price: '69000 &#8381', info: 'Сникеры B01 — классика мужского гардероба Dior.<br> Модель из гладкой телячьей кожи черного и белого цветов с черными замшевыми деталями'},
    {href: 'product18.html', text: 'Dior B25 Runer', images: ['b25_1.jpg','b25_2.jpg'], price: '80000 &#8381', info: 'В дизайне сникеров B25 Runner спортивный стиль сочетается с изысканной эстетикой модного дома.<br> Они выполнены из замши черного цвета и белой технической сетки'},
    {href: 'product19.html', text: 'DIOR EXPLORER', images: ['exp1.jpg','exp2.jpg'], price: '140000 &#8381', info: 'Ботинки Dior Explorer из черной гладкой телячьей кожи отличаются<br> вставками из жаккарда Dior Oblique бежевого и черного цветов по бокам.'},
    {href: 'product20.html', text: 'DIOR SNOW', images: ['snow1.jpg','snow2.jpg'], price: '140000 &#8381', info: 'Ботинки Dior Snow представляют собой модель в стиле apres-ski в новой интерпретации от-кутюр.<br> Модель голубого цвета выполнена из гладкой телячьей кожи и непромокаемой стеганой технической ткани'},
];
 
 function showModal(messageText, buttonText) {
    //alert('Sorry, not ready yet...\nИзвинте, еще не готово...');
    let modal = document.getElementsByClassName('modal')[0];
    modal.style.visibility = 'visible';
    modal.style.opacity = '1';
    modal.style.top = '50px';
    modal.style.left = '47%';
    modal.style.transform = 'scale(01';
    
    
 
 
    let message = modal.getElementsByClassName('message')[0];
    message.innerHTML = messageText;
    let button = modal.getElementsByTagName('button')[0];
    button.innerHTML = buttonText;
 
    document.body.style.overflow = 'hidden';
    let = overlay = document.getElementsByClassName('overlay')[0];
    overlay.style.visibility = 'visible';
    overlay.style.opacity = '1';
    overlay.style.backgroundcolor = 'rgba(0, 100, 200, 0.3)';
 
    return false;
 
 
 }
 function hideModal() {
     let modal = document.getElementsByClassName('modal')[0];
     setTimeout(function(){
         modal.style.visibility = 'hidden'
     }, 350);
     modal.style.opacity = '0';
     modal.style.top = '100%';
     modal.style.transform = 'scale(0)';
 
     document.body.style.overflow = 'auto';
     let = overlay = document.getElementsByClassName('overlay')[0];
     setTimeout(function(){
         overlay.style.visibility = 'hidden';
     }, 200);
     overlay.style.opacity = '0';
     overlay.style.backgroundcolor = 'rgba(255, 0, 0, 0.3)';
 
     
 }
 function notReadyAlert(event) {
     showModal('Sorry, not ready yet!<br>Извинте, еще не готово!', 'Эх, жаль');
     event.preventDefault();
     return false;
     }
 
 function notReadyAlert1() {
     showModal('Звоните, если угадаете)))0))))','Прости, друг');
    return false;
 }
 function notReadyAlert2() {
     showModal('Это наш сайт', 'НеНаСтОяЩиЙ');
    return false;
 }
 function notReadyAlert3() {
     showModal('Хмм... или не прикол', 'Да ладно, не важно, что я там сказал');
    return false;
 }
 
 function search() {
    let cards = document.getElementsByClassName('card');
    let name = document.getElementById('search').value;
    let nameRegExp = new RegExp(name, 'i');
    for (let i = 0; i<products.length; i++) {
       let product = products[i];
       if(nameRegExp.test(product.text)) {
          let card = cards[i];
          card.style.border = '3px dashed red';
          card.style.backgroundcolor = 'yellow';
 
          setTimeout(function(){
             card.style.border = 'none';
             card.style.backgroundcolor = '';
          }, 2000);
          
       }
    }
 }
 function generateMenu() {
    let menu = document.querySelector('nav.main-menu ul');
    menu.innerHTML = '';
    let items = [
       {href: 'index.html', text: 'Товары'},
       {href: 'contacts.html', text: 'Контакты'},
       {href: 'discont.html', text: 'Акции'},
    ];
 
    for(let i = 0; i<items.length; i++){
       let link = document.createElement('a');
       link.innerText = items[i].text;
       link.href = items[i].href;
       if (items[i].href == '') {
          link.addEventListener('click', notReadyAlert);
       }
 
       let menuItem = document.createElement('li');
       menuItem.appendChild(link);
 
       menu.appendChild(menuItem);
    }
 }
 function showProductInfo(product) {
    let imagesList = '';
    for(let i=0; i<product.images.length; i++){
        imagesList = imagesList + `<div><img src="${product.images[i]}"></div>`
    }
        showModal(`Информация о товаре
            ${imagesList}
            <div>${product.text}</div>
            <div>${product.price} &#8381; </div>
            <div><i>${product.info}</i></div>
            `, 'Спасибо');
        }
        function generateCard() {
            let main = document.querySelector('main');
            for(let i = 0; i<products.length; i++){
               let product = document.createElement('div');
               product.className= 'card';
               product.innerHTML = `
               <a href="#">
                  <div class="image"><img src="${products[i].images[0]}"></div>
                  <div class="product-name">"${products[i].text}"</div>
                  <div class="price" onmouseover="ColorPrice(this)" onmouseout="ColorPrice1(this)">${products[i].price}</div>
                  </a>
                  `;
               product.querySelector('a').addEventListener('click', function(){
                  showProductInfo(products[i]);
               });
         
               main.appendChild(product);
            }
         }
  function loaded() {
     let searchbox = document.getElementById('search');
     searchbox.addEventListener('keydown', function(key){
        if(key.key == 'Enter')
            search();
     });
 
     generateMenu();
     generateCard();
 
  }
  function ColorPrice(text){
     console.log('ПРивет');
     text.style.color = 'brown';
     text.style.fontWeight = 'blood';
  }
  function ColorPrice1(text){
     text.style.color = 'black';
     text.style.fontWeight = 'black';
  }


  